import bodyParser from 'body-parser';
import connectLivereload from 'connect-livereload';
import express from 'express';
import expressHbs from 'express-handlebars';
import livereload from 'livereload';
import { dirname } from 'path';
import { db } from './db/db.js';
import Handlers from './handlers/hnadlers.js';

const app = express();
const PORT = process.env.PORT || 4545;
const __dirname = dirname(new URL(import.meta.url).pathname);
const publicPath = __dirname + '/public';
const liveReloadServer = livereload.createServer();
const handlebars = expressHbs.create({
  defaultLayout: 'main',
  extname: 'hbs',
  helpers: {
    formatTime(date) {
      return new Date(date).toLocaleString('RU');
    }
  }
});
const handlers = new Handlers();

liveReloadServer.watch(publicPath);
liveReloadServer.server.once('connection', () => {
  setTimeout(() => {
    liveReloadServer.refresh('/')
  }, 1000)
});

app.use(connectLivereload());
app.use(express.static(publicPath));
app.use(bodyParser.urlencoded({ extended: true }));
app.use(express.json());
app.engine('hbs', handlebars.engine);
app.set('view engine', 'hbs');

app.route('/')
  .get(handlers.home)
  .post(handlers.createAd);
app.get('/create_ad', handlers.showAdForm);
app.get('/:category', handlers.showCategoryAd)

const startServer = async () => {
  try {
    app.listen(PORT, () => console.log(`Server start on http://localhost:${PORT}`));
    process.on("SIGINT", () => {
      db.close();
      process.exit();
    });
  } catch (error) {
    console.log(error);
  }
}

startServer()

process.on("SIGINT", () => {
  db.close();
  process.exit();
});