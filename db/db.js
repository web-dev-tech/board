import mongodb from 'mongodb';

const dbString = 'mongodb+srv://julius:oppenheimer@cluster0.ou1tc.mongodb.net/board?authSource=admin&replicaSet=atlas-xye5gr-shard-0&w=majority&readPreference=primary&appname=MongoDB%20Compass&retryWrites=true&ssl=true';
const mCLient = new mongodb.MongoClient(dbString, { useUnifiedTopology: true, useNewUrlParser: true });
let db, ads;

mCLient.connect(async (err, mongo) => {
  if (err) console.log(err);
  console.log('MongoDB: ready to use');
  db = mongo;
  ads = mongo.db('board').collection('ads');
});

export { db, ads };
