import { v4 as uuid4 } from 'uuid';
import { ads } from "../db/db.js";

export default class Handlers {
  async home(req, res) {
    try {
      const adsAll = await ads.find({}).toArray();
      res.render('home', { title: 'АБИБОК | Главная страница', ads: adsAll });
    } catch (error) {
      console.log(error);
    }
  }

  showAdForm(req, res) {
    res.render('create_ad', { title: 'Добавить объявление' });
  }

  async createAd(req, res) {
    try {
      const { title, category, text, author, email } = req.body;
      const _id = uuid4();
      const published = Date.now();
      const newAds = { _id, title, category, text, author, email, published };

      await ads.insertOne(newAds);

      res.render('create_ads', { message: 'Объявление опубликовано' });
    } catch (error) {
      res.status(400).send(error.message);
    }
  }

  async showCategoryAd(req, res) {
    try {
      const categoryAds = await ads.find({ category: req.params.category }).toArray();
      res.render('category', { title: 'Объявления', categoryAds });
    } catch (error) {
      res.status(400).send(error.message);
    }
  }
}